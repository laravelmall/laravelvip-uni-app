import request from '@/utils/request'

// api地址
const api = {
  list: '/user/address.html',
  defaultId: '/address/defaultId',
  detail: '/user/address/edit',
  add: '/user/address/add',
  edit: '/user/address/edit',
  setDefault: '/user/address/set-default',
  remove: '/user/address/del'
}

// 收货地址列表
export const list = (param) => {
  return request.get(api.list, param)
}

// 默认收货地址ID
export const defaultId = (param) => {
  return request.get(api.defaultId, param)
}

// 收货地址详情
export const detail = (addressId) => {
  return request.get(api.detail, { address_id:addressId })
}

// 新增收货地址
export const add = (data) => {
  return request.post(api.add, { form: data })
}

// 编辑收货地址
export const edit = (addressId, data) => {
  return request.post(api.edit, { addressId, form: data })
}

// 设置默认收货地址
export const setDefault = (addressId) => {
  return request.post(api.setDefault, { address_id:addressId })
}

// 删除收货地址
export const remove = (addressId) => {
  return request.post(api.remove, { address_id:addressId })
}
