import request from '@/utils/request'

// api地址
const api = {
  list: '/list.html',
	desc: '/goods/desc.html'
}

// 商品列表
export const list = param => {
  return request.get(api.list, param)
}

// 商品详情
export const detail = goodsId => {
  return request.get('/goods-'+goodsId+'.html')
}

// 商品图文内容
export function desc(sku_id) {
	return request.get(api.desc, {sku_id})
}
