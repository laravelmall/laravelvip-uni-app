import request from '@/utils/request'

// api地址
const api = {
  chatGpt: '/openai/chat-gpt'
}

// chat gpt
export const chatGpt = (data, option) => {
  return request.post(api.chatGpt, data, option)
}
